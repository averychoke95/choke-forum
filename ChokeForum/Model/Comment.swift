//
//  Comment.swift
//  ChokeForum
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import Foundation

class Comment {
    private static let nameKey = "CFFCommentText"

    var objectId: String?
    var text: String
    var date: Date
    var username: String = "Anonymous"

    init(text: String, date: Date) {
        self.text = text
        self.date = date
    }
    
    convenience init(text: String) {
        self.init(text: text, date: Date())
        self.username = User.instance.username
    }
}

extension Comment: Comparable {
    static func < (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.date < rhs.date
    }
    
    static func == (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.date == rhs.date
    }
    
    static func > (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.date > rhs.date
    }
}

//JSON API
extension Comment {
    convenience init?(json: [String : Any]) {
        if let text = json["text"] as? String, let created = json["created"] as? Double {
            self.init(text: text, date: Date(timeIntervalSince1970: created/1000))
        } else {
            return nil
        }
        
        if let objectId = json["objectId"] as? String {
            self.objectId = objectId
        }
        
        //obtain username of the person who wrote this comment
        if let ownerId = json["ownerId"] as? String {
            ForumLoader.sharedLoader.readFromServer(owner: ownerId) {
                (username: String?, error: Error?) -> Void in
                if let username = username {
                    self.username = username
                }
            }
        }
    }
    
    func jsonDict() -> [String : Any] {
        var jsonDict: [String : Any] = [
            "text" : self.text,
            "___class" : "Comment"]
        
        if let objectId = objectId {
            jsonDict["objectId"] = objectId
        }
        
        return jsonDict
    }
}
