//
//  Topic.swift
//  ChokeForum
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import Foundation
import UIKit

class Topic {
    private static let titleKey = "CFFTopicTitle"

    var objectId: String?
    var title: String
    var logo: UIImage?
    var logoURL: URL?
    var comments: [Comment] = []
    var ownerUsername: String = "Anonymous"

    var lastUpdatedDate: Date? {
        get {
            return comments.last?.date
        }
    }
    
    init(title: String) {
        self.title = title
        self.ownerUsername = User.instance.username
    }

    convenience init(title: String, logo: UIImage!) {
        self.init(title: title)
        self.logo = logo
    }
    
    func checkOwner(username: String) -> Bool {
        return username == ownerUsername
    }
}

extension Topic: Comparable {
    static func < (lhs: Topic, rhs: Topic) -> Bool {
        return lhs.title < rhs.title
    }
    
    static func == (lhs: Topic, rhs: Topic) -> Bool {
        return lhs.title == rhs.title
    }
    
    static func > (lhs: Topic, rhs: Topic) -> Bool {
        return lhs.title > rhs.title
    }
}

//JSON API
extension Topic {
    convenience init?(json: [String : Any]) {
        if let title = json["title"] as? String {
            self.init(title: title)
        } else {
            return nil
        }
        
        if let objectId = json["objectId"] as? String {
            self.objectId = objectId
        }
        if let logoURL = json["logo"] as? String {
            self.logoURL = URL(string: logoURL)
        }
        
        if let commentsJson = json["comments"] as? [[String : Any]] {
            for commentJson in commentsJson {
                if let aComment: Comment = Comment(json: commentJson) {
                    self.comments.append(aComment)
                }
            }
            //sort the comments
            self.comments.sort()
        }
    }
    
    func jsonDict() -> [String: Any] {
        var jsonDict: [String: Any] = ["title": self.title, "logo": ""]
        
        var commentsJson: [[String: Any]] = []
        for comment in comments {
            commentsJson.append(comment.jsonDict())
        }
        if !commentsJson.isEmpty {
            jsonDict["comments"] = commentsJson
        }
        
        return jsonDict
    }
}
