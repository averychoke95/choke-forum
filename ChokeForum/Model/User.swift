//
//  User.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 27/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    private static let objectIdKey = "CFFUserObjectId"
    private static let usernameKey = "CFFUserUsername"
    private static let userTokenKey = "CFFUserUserTokenKet"
    
    var objectId: String!
    var username: String!
    var userToken: String!
    
    static let instance: User = User()
    
    private override init() {
        super.init()
    }
    
    func initialize(username: String, objectId: String, userToken: String) {
        self.username = username
        self.objectId = objectId
        self.userToken = userToken
    }
    
    func initialize(user: User) {
        self.username = user.username
        self.objectId = user.objectId
        self.userToken = user.userToken
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        self.init()
        
        self.initialize(username: keyedUnarchiver.decodeObject(forKey: User.usernameKey) as! String,
                        objectId: keyedUnarchiver.decodeObject(forKey: User.objectIdKey) as! String,
                        userToken: keyedUnarchiver.decodeObject(forKey: User.userTokenKey) as! String)
    }
    
    func encode(with aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encode(self.username, forKey: User.usernameKey)
        keyedArchiver.encode(self.objectId, forKey: User.objectIdKey)
        keyedArchiver.encode(self.userToken, forKey: User.userTokenKey)
    }
}
