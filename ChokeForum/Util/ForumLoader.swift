//
//  ForumLoader.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 24/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import Foundation
import UIKit

class ForumLoader {
    let session: URLSession
    let baseURL: URL
    
    let userDefaults: UserDefaults
    
    static let sharedLoader = ForumLoader()
    
    private init() {
        let config: URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        self.session = URLSession(configuration: config)
        self.baseURL = URL(string: "https://api.backendless.com/v1/")!
        
        self.userDefaults = UserDefaults.standard
    }
}

//operation on dealing with account of remote server
extension ForumLoader {
    func logIn(username: String, password: String, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        let loginURL: URL = URL(string: "users/login", relativeTo: self.baseURL)!
        
        let jsonDict: [String: Any] = ["login": username, "password": password]
        var jsonData: Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDict)
        } catch let jsonError {
            successBlock?(false, jsonError as Error)
            return
        }
        
        var urlRequest: URLRequest = URLRequest(url: loginURL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let loginTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                DispatchQueue.main.async {
                    successBlock?(false, nil)
                }
                return
            }
            
            let json : [String : Any] = try! JSONSerialization.jsonObject(with: data!) as! [String : Any]
            
            //check if login sucess
            //got user-token means success
            if let userToken = json["user-token"] as? String {
                User.instance.initialize(username: json["username"] as! String,
                                         objectId: json["objectId"] as! String,
                                         userToken: userToken)
                
                self.saveUserToDefaults()
                
                DispatchQueue.main.async {
                    successBlock?(true, nil)
                }
            } else {
                DispatchQueue.main.async {
                    successBlock?(false, nil)
                }
            }
        }
        
        loginTask.resume()
    }
    
    func isLoggedIn(successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        let userToken: String = User.instance.userToken
        let tokenURL: URL = URL(string: "users/isvalidusertoken/\(userToken)", relativeTo: self.baseURL)!
        
        let checkTask: URLSessionDataTask = self.session.dataTask(with: tokenURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if let error = error {
                DispatchQueue.main.async {
                    successBlock?(false, error)
                }
            } else {
                let successString: String = String(data: data!, encoding: String.Encoding.utf8)!
                let success:Bool = Bool(successString)!
                DispatchQueue.main.async {
                    successBlock?(success, nil)
                }
            }
        }
        
        checkTask.resume()
    }
    
    func logOut(successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let logoutURL: URL = URL(string: "users/logout", relativeTo: self.baseURL)!
        
        var urlRequest: URLRequest = URLRequest(url: logoutURL)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let logoutTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            self.deleteUserFromDefaults()
            
            successBlock?(data != nil, nil)
        }
        
        logoutTask.resume()
    }
    
    func signUp(username: String, password: String, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        let signupURL: URL = URL(string: "users/register", relativeTo: self.baseURL)!
        
        let jsonDict: [String: Any] = ["username": username, "password": password]
        var jsonData: Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDict)
        } catch let jsonError {
            successBlock?(false, jsonError as Error)
            return
        }
        
        var urlRequest: URLRequest = URLRequest(url: signupURL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let signupTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                DispatchQueue.main.async {
                    successBlock?(false, nil)
                }
                return
            }
            
            let json : [String : Any] = try! JSONSerialization.jsonObject(with: data!) as! [String : Any]
            
            //check if sign up sucess
            //got objectId means success
            if let _ = json["objectId"] as? String {
                DispatchQueue.main.async {
                    successBlock?(true, nil)
                }
            } else {
                DispatchQueue.main.async {
                    successBlock?(false, nil)
                }
            }
        }
        
        signupTask.resume()
    }
}

//operation on saving/updating/reading data from remote server
extension ForumLoader {
    func readAllTopicsFromServer(completionBlock: ((_ topics: [Topic], _ error: Error?) -> Void)?) {
        let topicURL: URL = URL(string: "data/Topic", relativeTo: self.baseURL)!
        
        let readTask: URLSessionDataTask = self.session.dataTask(with: topicURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                completionBlock?([], nil)
                return
            }
            
            var json: [String: Any]!
            do {
                json = try JSONSerialization.jsonObject(with: data!) as! [String: Any]
            } catch let jsonError {
                completionBlock?([], jsonError as Error)
                return
            }
            
            let totalObjects: Int = json["totalObjects"] as! Int
            
            var serverTopics: [Topic] = []
            if let topicsJson: [[String: Any]] = json["data"] as? [[String: Any]] {
                for i in 0...(totalObjects-1) {
                    let topicJson: [String: Any] = topicsJson[i]
                    
                    if let topic: Topic = Topic(json: topicJson) {
                        serverTopics.append(topic)
                        
                        //get the owner of the topic
                        if let ownerId: String = topicJson["ownerId"] as? String {
                            self.readFromServer(owner: ownerId) {
                                (username: String?, error: Error?) -> Void in
                                if let username = username {
                                    topic.ownerUsername = username
                                }
                                
                                //call the completion block after everyone finished obtaining the username
                                if i == totalObjects-1 {
                                    DispatchQueue.main.async {
                                        completionBlock?(serverTopics, nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        readTask.resume()
    }
    
    func readFromServer(topic: Topic, completionBlock: ((_ topic: Topic?, _ error: Error?) -> Void)?) {
        let topicURL: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: self.baseURL)!
        
        let readTask: URLSessionDataTask = self.session.dataTask(with: topicURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                completionBlock?(nil, nil)
                return
            }
            
            var json: [String: Any]!
            do {
                json = try JSONSerialization.jsonObject(with: data!) as! [String: Any]
            } catch let jsonError {
                completionBlock?(nil, jsonError as Error)
                return
            }
            
            if let myTopic: Topic = Topic(json: json) {
                myTopic.ownerUsername = topic.ownerUsername
                
                DispatchQueue.main.async {
                    completionBlock?(myTopic, nil)
                }
            } else {
                completionBlock?(nil, nil)
            }
        }
        
        readTask.resume()
    }
    
    func readFromServer(owner ownerId: String, completionBlock: ((_ username: String?, _ error: Error?) -> Void)?) {
        let ownerURL: URL = URL(string: "data/Users/\(ownerId)", relativeTo: self.baseURL)!
        
        let readTask: URLSessionDataTask = self.session.dataTask(with: ownerURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                completionBlock?(nil, nil)
                return
            }
            
            var json: [String: Any]!
            do {
                json = try JSONSerialization.jsonObject(with: data!) as! [String: Any]
            } catch let jsonError {
                completionBlock?(nil, jsonError as Error)
                return
            }
            
            if let username: String = json["username"] as? String {
                completionBlock?(username, nil)
            } else {
                completionBlock?(nil, nil)
            }
        }
        
        readTask.resume()
    }
    
    func loadImageTask(imageURL: URL, completionBlock: ((_ image: UIImage?, _ error: Error?) -> Void)?) -> URLSessionDataTask {
        var urlRequest: URLRequest = URLRequest(url: imageURL)
        urlRequest.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
        
        let loadTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (imageData: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if imageData == nil {
                completionBlock?(nil, nil)
                return
            }
            
            var image: UIImage?
            if let imageData = imageData {
                image = UIImage(data: imageData)
            }
            
            DispatchQueue.main.async {
                completionBlock?(image, error)
            }
        }
        
        return loadTask
    }
    
    func saveToServer(topic: Topic, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let topicUrl: URL = URL(string: "data/Topic", relativeTo: self.baseURL)!
        
        var jsonData: Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: topic.jsonDict())
        } catch let jsonError {
            successBlock?(false, jsonError as Error)
            return
        }
        
        var urlRequest: URLRequest = URLRequest(url: topicUrl)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let writeTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                successBlock?(false, nil)
                return
            }
            
            //get object id and update the topic
            let json : [String : Any] = try! JSONSerialization.jsonObject(with: data!) as! [String : Any]
            topic.objectId = json["objectId"] as? String
            
            //save the logo if present
            if topic.logo != nil {
                self.saveToServer(logoOfTopic: topic, successBlock: successBlock)
            } else {
                successBlock?(true, nil)
            }
        }
        
        writeTask.resume()
    }
    
    func saveToServer(logoOfTopic topic: Topic, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let logoURL: URL = URL(string: "files/logo/\(topic.objectId!).jpg", relativeTo: self.baseURL)!
        
        //set the url for the topic so that it can be deleted from server
        topic.logoURL = logoURL
        
        let imageData: Data = UIImageJPEGRepresentation(topic.logo!, 0.5)!
        
        var urlRequest: URLRequest = URLRequest(url: logoURL)
        urlRequest.httpMethod = "POST"
        
        let (boundary, formData) = HTTPContentTypeUtil.convertToFormData(fileName: "logo.jpg", mimeType: "image/jpeg", fileData: imageData)
        urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let uploadTask: URLSessionDataTask = self.session.uploadTask(with: urlRequest, from: formData) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                successBlock?(false, nil)
                return
            }
            
            var json: [String: Any] = try! JSONSerialization.jsonObject(with: data!) as! [String: Any]
            
            if let fileURL:String = json["fileURL"] as? String{
                //update saved topic logo
                self.updateInServer(logoOfTopic: topic, logoStringUrl: fileURL, successBlock: successBlock)
            } else {
                successBlock?(false, nil)
            }
        }
        
        uploadTask.resume()
    }
    
    func saveToServer(comment: Comment, of topic: Topic, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let topicURL: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: self.baseURL)!
        
        let jsonDict: [String : Any] = ["comments" : comment.jsonDict()]
        
        var jsonData: Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDict)
        } catch let jsonError {
            successBlock?(false, jsonError as Error)
            return
        }
        
        var urlRequest: URLRequest = URLRequest(url: topicURL)
        urlRequest.httpMethod = "PUT"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let writeTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                successBlock?(false, nil)
                return
            }
            
            //get object id and update the comment
            let json: [String: Any] = try! JSONSerialization.jsonObject(with: data!) as! [String: Any]
            let commentJson: [String: Any] = (json["comments"] as! [[String: Any]]).first!
            comment.objectId = commentJson["objectId"] as? String
            
            successBlock?(true, nil)
        }
        
        writeTask.resume()
    }
    
    func updateInServer(topic: Topic, deletedComments: [Comment] = [], successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        //obtain meta for relationship
        self.getMetaFromServer(topic: topic) {
            (meta: String?, error: Error?) -> Void in
            let topicURL: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: self.baseURL)!
            
            var jsonDict: [String : Any] = topic.jsonDict()
            if let meta = meta {
                jsonDict["__meta"] = meta
            }
            
            var jsonData: Data!
            do {
                jsonData = try JSONSerialization.data(withJSONObject: jsonDict)
            } catch let jsonError {
                successBlock?(false, jsonError as Error)
                return
            }
            
            var urlRequest: URLRequest = URLRequest(url: topicURL)
            urlRequest.httpMethod = "PUT"
            urlRequest.httpBody = jsonData
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
            
            //change all value in server
            let updateTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
                (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
                successBlock?(data != nil, nil)
            }
            
            updateTask.resume()
        }
        
        //delete old logo
        if let logoURL: URL = topic.logoURL {
            self.deleteFromServer(file: logoURL) {
                (succeeded: Bool, error: Error?) -> Void in
                if succeeded && topic.logo != nil {
                    //save new logo into server
                    self.saveToServer(logoOfTopic: topic)
                }
            }
        } else if topic.logo != nil {
            self.saveToServer(logoOfTopic: topic)
        }
        
        //delete removed comments
        for comment in deletedComments {
            self.deleteFromServer(comment: comment)
        }
    }
    
    func updateInServer(logoOfTopic topic: Topic, logoStringUrl: String, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let topicLogoURL: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: self.baseURL)!
        
        let jsonDict: [String : Any] = ["logo" : logoStringUrl]
        var jsonData: Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDict)
        } catch let jsonError {
            successBlock?(false, jsonError as Error)
            return
        }
        
        var urlRequest: URLRequest = URLRequest(url: topicLogoURL)
        urlRequest.httpMethod = "PUT"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let updateTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            successBlock?(data != nil, nil)
        }
        
        updateTask.resume()
    }
    
    func deleteFromServer(topic: Topic, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let topicURL: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: self.baseURL)!
        
        var urlRequest: URLRequest = URLRequest(url: topicURL)
        urlRequest.httpMethod = "DELETE"
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        //delete the topic from server
        let deleteTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                successBlock?(false, nil)
                return
            }
            
            //delete the comments from server
            for comment in topic.comments {
                self.deleteFromServer(comment: comment)
            }
            
            //delete the logo from server
            if let logoURL = topic.logoURL {
                self.deleteFromServer(file: logoURL)
            }
            
            successBlock?(true, nil)
        }
        
        deleteTask.resume()
    }
    
    func deleteFromServer(comment: Comment, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        let commentURL: URL = URL(string: "data/Comment/\(comment.objectId!)", relativeTo: self.baseURL)!
        
        var urlRequest: URLRequest = URLRequest(url: commentURL)
        urlRequest.httpMethod = "DELETE"
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let deleteTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            successBlock?(data != nil, nil)
        }
        
        deleteTask.resume()
    }
    
    func deleteFromServer(file fileURL: URL, successBlock: ((_ succeeded: Bool, _ error: Error?) -> Void)? = nil) {
        //create empty json
        let jsonDict: [String: Any] = [String: Any]()
        var jsonData: Data!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDict)
        } catch let jsonError {
            successBlock?(false, jsonError as Error)
            return
        }
        
        var urlRequest: URLRequest = URLRequest(url: fileURL)
        urlRequest.httpMethod = "DELETE"
        urlRequest.httpBody = jsonData
        urlRequest.addValue(User.instance.userToken, forHTTPHeaderField: "user-token")
        
        let deleteTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            successBlock?(data != nil, nil)
        }
        
        deleteTask.resume()
    }
    
    func getMetaFromServer(topic: Topic, completionBlock: ((_ meta: String?, _ error: Error?) -> Void)?) {
        let topicURL: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: self.baseURL)!
        
        let readTask: URLSessionDataTask = self.session.dataTask(with: topicURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if data == nil {
                completionBlock?(nil, nil)
                return
            }
            
            var json: [String: Any]!
            do {
                json = try JSONSerialization.jsonObject(with: data!) as! [String: Any]
            } catch let jsonError {
                completionBlock?("", jsonError as Error)
                return
            }
            
            let meta: String = json["__meta"] as! String
            
            completionBlock?(meta, nil)
        }
        
        readTask.resume()
    }
}

//operation on saving/reading data from local device
extension ForumLoader {
    private static let userDataKey = "CFFUserData"
    
    func initializeUserFromDefaults() -> Bool {
        if let userData: Data = userDefaults.object(forKey: ForumLoader.userDataKey) as? Data {
            let user: User = NSKeyedUnarchiver.unarchiveObject(with: userData) as! User

            User.instance.initialize(user: user)
            
            return true
        } else {
            return false
        }
    }
    
    func saveUserToDefaults() {
        let userData: Data = NSKeyedArchiver.archivedData(withRootObject: User.instance)
        self.userDefaults.set(userData, forKey: ForumLoader.userDataKey)
        self.userDefaults.synchronize()
    }
    
    func deleteUserFromDefaults() {
        self.userDefaults.removeObject(forKey: ForumLoader.userDataKey)
        self.userDefaults.synchronize()
    }
}
