//
//  HTTPContentTypeUtil.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 25/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import Foundation

class HTTPContentTypeUtil {

    static func convertToFormData(fileName: String, mimeType: String, fileData: Data) -> (boundary: String, formData: Data) {
        let boundary: String = UUID().uuidString
        var formData: Data = Data()
        
        formData.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        formData.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fileName)\"\r\n".data(using: String.Encoding.utf8)!)
        formData.append("Content-Type: \(mimeType)\r\n\r\n".data(using: String.Encoding.utf8)!)
        formData.append(fileData)
        formData.append("\r\n".data(using: String.Encoding.utf8)!)
        formData.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        return (boundary, formData)
    }
    
    
}
