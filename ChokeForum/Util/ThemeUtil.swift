//
//  Theme.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 30/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import Foundation
import UIKit

enum Theme: Int {
    case light, dark, green
    
    var tintColor: UIColor {
        switch self {
        case Theme.light:
            return UIColor(red: 0, green: 122.0/255.0, blue: 1, alpha: 1)
        case Theme.dark:
            return UIColor.black
        case Theme.green:
            return UIColor(red: 12.0/255.0, green: 112.0/255.0, blue: 0, alpha: 1)
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case Theme.light:
            return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        case Theme.dark:
            return UIColor.lightGray
        case Theme.green:
            return UIColor(red: 67.0/255.0, green: 221.0/255.0, blue: 65.0/255.0, alpha: 0.8)
        }
    }
}

struct ThemeUtil {
    static let themeKey: String = "CFFColorTheme"
    static let userDefaults: UserDefaults = UserDefaults.standard
    
    static func currentTheme() -> Theme {
        let value:Int = self.userDefaults.integer(forKey: self.themeKey)
        return Theme(rawValue: value)!
    }
    
    static func applyTheme(theme: Theme) {
        //change the UI
        let sharedApplication: UIApplication = UIApplication.shared
        sharedApplication.delegate?.window??.tintColor = theme.tintColor
        sharedApplication.delegate?.window??.backgroundColor = theme.backgroundColor
    }
}
