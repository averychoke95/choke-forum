//
//  AddTopicViewController.swift
//  ChokeForum
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit

class AddTopicViewController: UIViewController {


    @IBOutlet weak var topicTitleTextField: UITextField!
    @IBOutlet weak var logoImageView: UIImageView!

    var topicAddedBlock: ((_ topic: Topic) -> Void)?

    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func uploadPicture(_ sender: Any) {
        self.present(imagePicker, animated: true, completion: nil)
    }

    @IBAction func removePicture(_ sender: Any) {
        self.logoImageView.image = UIImage(named: "no_image")!
    }
    
    @IBAction func addTopic(_ sender: Any) {
        if !topicTitleTextField.text!.isEmpty {
            let newTopic: Topic = Topic(title: topicTitleTextField.text!)
            if self.logoImageView.image != UIImage(named: "no_image")! {
                newTopic.logo = self.logoImageView.image
            }

            //save into the server
            ForumLoader.sharedLoader.saveToServer(topic: newTopic)
            
            self.topicAddedBlock?(newTopic)
        }
    }

    @IBAction func cancelCreation(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension AddTopicViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddTopicViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let logoImage:UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.logoImageView.image = logoImage
        }
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
