//
//  TopicEditViewController.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 26/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit

class EditTopicViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleEditField: UITextField!
    @IBOutlet weak var commentTableView: UITableView!
    
    var topic: Topic?
    var topicDeletedBlock: (() -> Void)?
    
    let imagePicker = UIImagePickerController()
    
    //store a list of indexes that need to be removed for the comments
    var removingCommentIndexes: Set<Int> = Set<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let _ = topic else {
            _ = self.navigationController?.popViewController(animated: false)
            return
        }
        
        if let logo = self.topic!.logo {
            self.logoImageView.image = logo
        }
        
        self.titleEditField.text = topic!.title
        
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        let uploadImageTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(uploadImage))
        uploadImageTap.numberOfTapsRequired = 1
        self.logoImageView.addGestureRecognizer(uploadImageTap)
        
        let deleteImageTap: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(deleteImage))
        self.logoImageView.addGestureRecognizer(deleteImageTap)
        uploadImageTap.require(toFail: deleteImageTap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backToTopicDetailVC(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteTopic(_ sender: Any) {
        let alert = UIAlertController(title: "Delect Confirmation", message: "You are about to delete this topic. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            (action) -> Void in
            //delete from server
            ForumLoader.sharedLoader.deleteFromServer(topic: self.topic!)
            
            self.topicDeletedBlock?()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func updateEdit(_ sender: Any) {
        if !(titleEditField.text!.isEmpty) {
            self.topic!.title = self.titleEditField.text!
            
            if self.logoImageView.image != UIImage(named: "no_image")! {
                self.topic!.logo = self.logoImageView.image
            } else {
                self.topic!.logo = nil
            }
            
            var deletedComments: [Comment] = []
            for indexes in self.removingCommentIndexes {
                deletedComments.append(self.topic!.comments.remove(at: indexes))
            }
            
            //update in server
            ForumLoader.sharedLoader.updateInServer(topic: self.topic!, deletedComments: deletedComments)
            
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func uploadImage(sender: UITapGestureRecognizer) {
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func deleteImage(sender: UILongPressGestureRecognizer) {
        let noImage: UIImage = UIImage(named: "no_image")!
        if self.logoImageView.image != noImage {
            let alert = UIAlertController(title: nil, message: "Remove this image?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
                (action) -> Void in
                self.logoImageView.image = noImage
            })
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension EditTopicViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let topic = topic {
            return topic.comments.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = commentTableView.dequeueReusableCell(withIdentifier: "CommentTableCell", for: indexPath)
        
        let comment = topic!.comments[indexPath.row]
        cell.textLabel?.text = comment.text
        cell.detailTextLabel?.text = "By \(comment.username) at \(comment.date)"
        
        if self.removingCommentIndexes.contains(indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        
        return cell
    }
}

extension EditTopicViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.removingCommentIndexes.contains(indexPath.row) {
            self.removingCommentIndexes.remove(indexPath.row)
        } else {
            self.removingCommentIndexes.insert(indexPath.row)
        }
        
        self.commentTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
}

extension EditTopicViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension EditTopicViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let logoImage:UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.logoImageView.image = logoImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension EditTopicViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
