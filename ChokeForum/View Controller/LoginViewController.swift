//
//  LoginViewController.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 27/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //define the color theme
        UserDefaults.standard.register(defaults: [String: Any]())
        ThemeUtil.applyTheme(theme: ThemeUtil.currentTheme())
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTheme), name: UserDefaults.didChangeNotification, object: nil)
        
        //check if user is already logged in previously
        if ForumLoader.sharedLoader.initializeUserFromDefaults() {
            let hud: MBProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.label.text = "Logging In"
            
            //check if the log in is still valid
            ForumLoader.sharedLoader.isLoggedIn() {
                (successed: Bool, error: Error?) -> Void in
                hud.hide(animated: true)
                
                self.logInSucess(successed: successed, error: error)
                
                //log in no longer valid - delete the user data
                if !successed {
                    ForumLoader.sharedLoader.deleteUserFromDefaults()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.usernameTextField.text = ""
        self.passwordTextField.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showSignUpVC(_ sender: Any) {
        let signUpVC: SignUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        
        signUpVC.signupSuccessBlock = {
            () -> Void in
            let successHUD: MBProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            successHUD.mode = MBProgressHUDMode.text
            successHUD.label.text = "Signed Up Successfully"
            successHUD.offset = CGPoint(x: 0.1, y: MBProgressMaxOffset)
            successHUD.hide(animated: true, afterDelay: 3)
        }
        
        self.navigationController?.present(signUpVC, animated: true, completion: nil)
    }
    
    
    @IBAction func checkLogIn(_ sender: Any) {
        let username: String = usernameTextField.text!
        let password: String = passwordTextField.text!
        
        if !username.isEmpty && !password.isEmpty {
            let hud: MBProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.label.text = "Logging In"
            
            ForumLoader.sharedLoader.logIn(username: username, password: password) {
                (successed: Bool, error: Error?) -> Void in
                hud.hide(animated: true)
                
                self.logInSucess(successed: successed, error: error)
            }
        } else {
            let alert = UIAlertController(title: "Log In Failed", message: "Username and/or password must not be empty", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func logInSucess(successed: Bool, error: Error?) {
        if successed {
            //show topicListVC
            let topicListVC: TopicListViewController = self.storyboard?.instantiateViewController(withIdentifier: "TopicListViewController") as! TopicListViewController
            
            self.navigationController?.pushViewController(topicListVC, animated: true)
        } else {
            //pop up error
            let alert = UIAlertController(title: "Log In Failed", message: "Wrong username and/or password or please check ur connection", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func updateTheme() {
        ThemeUtil.applyTheme(theme: ThemeUtil.currentTheme())
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.usernameTextField {
            self.passwordTextField.becomeFirstResponder()
        } else if textField == self.passwordTextField {
            self.passwordTextField.resignFirstResponder()
        }
        return true
    }
}
