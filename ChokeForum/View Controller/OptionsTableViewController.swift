//
//  SettingTableViewController.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 29/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit

class OptionsTableViewController: UITableViewController {

    let options: [String] = ["Setting", "Logout"]
    
    var parentVC: UIViewController?
    
    override var preferredContentSize: CGSize {
        get {
            var tempSize: CGSize = self.presentingViewController!.view.bounds.size
            tempSize.width = 150
            let size: CGSize = self.tableView.sizeThatFits(tempSize)
            return size
        }
        set {
            super.preferredContentSize = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let _ = parentVC else {
            _ = self.dismiss(animated: false)
            return
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        
        return self.options.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "optionCell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "optionCell")
        }
        
        cell!.textLabel?.text = options[indexPath.row]

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option: String = options[indexPath.row]
        
        if option == "Setting" {
            let settingTVC: SettingTableViewController = parentVC!.storyboard?.instantiateViewController(withIdentifier: "SettingTableViewController") as! SettingTableViewController
            
            self.parentVC!.navigationController?.pushViewController(settingTVC, animated: true)
        } else if option == "Logout" {
            ForumLoader.sharedLoader.logOut()

            _ = self.parentVC!.navigationController?.popViewController(animated: true)
        }
        
        self.dismiss(animated: false)
    }
}
