//
//  SettingTableViewController.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 30/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {
    
    @IBOutlet weak var colorThemeSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateSegment()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateSegment), name: UserDefaults.didChangeNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    @IBAction func changeColorTheme(_ sender: Any) {
        let newIndex: Int = self.colorThemeSegment.selectedSegmentIndex
        
        UserDefaults.standard.set(newIndex, forKey: "CFFColorTheme")
    }
    
    func updateSegment() {
        self.colorThemeSegment.selectedSegmentIndex = ThemeUtil.currentTheme().rawValue
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
