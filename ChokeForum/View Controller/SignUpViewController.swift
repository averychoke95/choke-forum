//
//  SignUpViewController.swift
//  ChokeForum
//
//  Created by Avery Choke Kar Sing on 27/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit
import MBProgressHUD

class SignUpViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var signupSuccessBlock: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUp(_ sender: Any) {
        let username: String = usernameTextField.text!
        let password: String = passwordTextField.text!
        
        if !username.isEmpty && !password.isEmpty {
            let signupHUD: MBProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            signupHUD.label.text = "Signing Up"
            
            ForumLoader.sharedLoader.signUp(username: username, password: password) {
                (successed: Bool, error: Error?) -> Void in
                signupHUD.hide(animated: true)
                
                if successed {
                    //go back to loginVC
                    self.dismiss(animated: true, completion: self.signupSuccessBlock)
                } else {
                    //pop up error
                    let alert = UIAlertController(title: "Sign Up Failed", message: "Invalid username and/or password or please check your connection", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        } else {
            let alert = UIAlertController(title: "Sign Up Failed", message: "Username and/or password must not be empty", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func backToLoginVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.usernameTextField {
            self.passwordTextField.becomeFirstResponder()
        } else if textField == self.passwordTextField {
            self.passwordTextField.resignFirstResponder()
        }
        return true
    }
}
