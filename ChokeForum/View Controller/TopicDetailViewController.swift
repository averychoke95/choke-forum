//
//  TopicDetailViewController.swift
//  ChokeForum
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit
import MBProgressHUD

class TopicDetailViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topicOwnerLabel: UILabel!
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    var topic: Topic?
    var topicDeletedBlock: (() -> Void)?
    
    var refreshControl: UIRefreshControl!
    var loadImageTask: URLSessionDataTask?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let _ = topic else {
            _ = self.navigationController?.popViewController(animated: false)
            return
        }
        
        self.navigationItem.title = User.instance.username
        self.topicOwnerLabel.text = "Created by: " + topic!.ownerUsername
        
        //disable the edit button if the user is not the owner
        if !topic!.checkOwner(username: User.instance.username) {
            self.editButton.isEnabled = false
        }

        let showFullTitleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showFullTitle))
        showFullTitleTap.numberOfTapsRequired = 2
        titleLabel.addGestureRecognizer(showFullTitleTap)
        
        self.hideKeyboardWhenTappedAround()
        self.registerForMoveContentWhenKeyboardAppear()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        self.commentTableView.addSubview(refreshControl)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.refreshView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unregisterForMoveContentWhenKeyboardAppear()
    }

    @IBAction func showTopicEditVC(_ sender: Any) {
        let editTopicVC: EditTopicViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditTopicViewController") as! EditTopicViewController
        
        editTopicVC.topic = topic
        editTopicVC.topicDeletedBlock = {
            () -> Void in
            self.topicDeletedBlock?()
            
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        self.navigationController?.pushViewController(editTopicVC, animated: true)
    }

    @IBAction func addComment(_ sender: Any) {
        if !(commentTextView.text!.isEmpty) {
            let aComment: Comment = Comment(text: commentTextView.text!)
            //add comment
            topic!.comments.append(aComment)
            
            //save comment into server
            ForumLoader.sharedLoader.saveToServer(comment: aComment, of: self.topic!)
            
            //refresh text view
            commentTextView.text = ""
            //hide keyboard
            commentTextView.resignFirstResponder()
            
            //update table view
            commentTableView.reloadData()
        } else {
            let messageHUD: MBProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            messageHUD.mode = MBProgressHUDMode.text
            messageHUD.label.text = "Unable put an empty comment"
            messageHUD.offset = CGPoint(x: 0.1, y: MBProgressMaxOffset)
            messageHUD.hide(animated: true, afterDelay: 2)
        }
    }

    func showFullTitle(sender: UITapGestureRecognizer) {
        let fullTitleAlert: UIAlertController = UIAlertController(title: nil, message: topic!.title, preferredStyle: UIAlertControllerStyle.alert)
        fullTitleAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(fullTitleAlert, animated: true, completion: nil)
    }
    
    func refresh() {
        ForumLoader.sharedLoader.readFromServer(topic: self.topic!) {
            (topic: Topic?, error: Error?) -> Void in
            if let topic = topic {
                self.topic = topic
                sleep(1)
                self.refreshView()
            }
            
            self.refreshControl.endRefreshing()
        }
    }
    
    func refreshView() {
        if let logo = self.topic!.logo {
            self.logoImageView.image = logo
        } else if let logoURL = self.topic!.logoURL {
            self.loadImageTask?.cancel()
            
            self.loadImageTask = ForumLoader.sharedLoader.loadImageTask(imageURL: logoURL) {
                (logoImage: UIImage?, error: Error?) -> Void in
                if let logoImage = logoImage {
                    self.topic!.logo = logoImage
                    self.logoImageView.image = logoImage
                } else {
                    self.logoImageView.image = #imageLiteral(resourceName: "no_image")
                }
            }
            
            self.loadImageTask?.resume()
        } else {
            self.logoImageView.image = #imageLiteral(resourceName: "no_image")
        }
        
        self.titleLabel.text = topic!.title
        
        self.commentTableView.reloadData()
    }
}

extension TopicDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let topic = topic {
            return topic.comments.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = commentTableView.dequeueReusableCell(withIdentifier: "CommentTableCell", for: indexPath)

        let comment = topic!.comments[indexPath.row]
        cell.textLabel?.text = comment.text
        cell.detailTextLabel?.text = "By \(comment.username) at \(comment.date)"

        return cell
    }
}

extension TopicDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//add functionailites to the UIiewController
extension UIViewController {
    //functions for hiding the keyboard when tapped outside
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    //
    
    //functions for swifting the textfield/textview upwards when keyboard is covering it
    func registerForMoveContentWhenKeyboardAppear() {
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unregisterForMoveContentWhenKeyboardAppear() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
