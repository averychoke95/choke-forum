//
//  TopicListViewController.swift
//  ChokeForum
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit
import MBProgressHUD

class TopicListViewController: UIViewController {

    @IBOutlet weak var topicTableView: UITableView!
    var optionsTableVC: OptionsTableViewController!

    var topics: [Topic] = []
    
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = User.instance.username
        
        self.refreshTopics()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(refreshTable), for: UIControlEvents.valueChanged)
        self.topicTableView.addSubview(refreshControl)
        
        self.optionsTableVC = OptionsTableViewController()
        self.optionsTableVC.modalPresentationStyle = UIModalPresentationStyle.popover
        self.optionsTableVC.parentVC = self
        
        self.navigationItem.rightBarButtonItem?.title = "\u{22EE}"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.topics.sort()
        self.topicTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showAddTopicVC(_ sender: Any) {
        let addTopicVC: AddTopicViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTopicViewController") as! AddTopicViewController

        addTopicVC.topicAddedBlock = {
            (topic: Topic) -> Void in
            self.topics.append(topic)
            self.topicTableView.reloadData()
            self.dismiss(animated: true, completion: nil)
        }

        self.navigationController?.present(addTopicVC, animated: true, completion: nil)
    }
    
    @IBAction func popoverOptions(_ sender: Any) {
        self.optionsTableVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        self.optionsTableVC.popoverPresentationController?.delegate = self
        self.navigationController?.present(self.optionsTableVC, animated: true)
    }
    
    func refreshTable() {
        self.refreshTopics(showHUD: false) {
            () -> Void in
            self.refreshControl.endRefreshing()
        }
    }
    
    func refreshTopics(showHUD: Bool = true, completionBlock: (() -> Void)? = nil) {
        var hud: MBProgressHUD?
        if showHUD {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud!.label.text = "Loading"
        }
        
        ForumLoader.sharedLoader.readAllTopicsFromServer(completionBlock: {
            (topics: [Topic], error: Error?) -> Void in
            if let error = error {
                NSLog(error as! String)
            } else {
                self.topics = topics
                self.topics.sort()
                self.topicTableView.reloadData()
                completionBlock?()
            }
            
            hud?.hide(animated: true)
        })
    }
}

extension TopicListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.topics.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TopicTableViewCell = self.topicTableView.dequeueReusableCell(withIdentifier: "TopicTableViewCell", for: indexPath) as! TopicTableViewCell

        cell.topic = topics[indexPath.row]

        return cell
    }
}

extension TopicListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let topicDetailVC: TopicDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "TopicDetailViewController") as! TopicDetailViewController

        topicDetailVC.topic = topics[indexPath.row]
        topicDetailVC.topicDeletedBlock = {
            () -> Void in
            self.topics.remove(at: indexPath.row)
            
            _ = self.navigationController?.popViewController(animated: false)
        }

        self.navigationController?.pushViewController(topicDetailVC, animated: true)
    }
}

extension TopicListViewController: UIAdaptivePresentationControllerDelegate, UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
