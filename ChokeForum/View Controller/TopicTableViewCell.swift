//
//  TopicTableViewCell.swift
//  ChokeForum
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!

    var loadImageTask: URLSessionDataTask?
    
    var topic: Topic! {
        didSet {
            updateDisplay()
        }
    }

    func updateDisplay() {
        self.tittleLabel.text = self.topic.title
        
        if let lastUpdatedDate = self.topic.lastUpdatedDate {
            self.subLabel.text = "Created by: \(topic.ownerUsername)\nLast updated: \(lastUpdatedDate)"
        } else {
            self.subLabel.text = "Created by: \(topic.ownerUsername)"
        }
        
        if let logo = self.topic.logo {
            self.logoImageView.image = logo
        } else if let logoURL = self.topic.logoURL {
            self.loadImageTask?.cancel()
            
            self.loadImageTask = ForumLoader.sharedLoader.loadImageTask(imageURL: logoURL) {
                (logoImage: UIImage?, error: Error?) -> Void in
                if let logoImage = logoImage {
                    self.topic.logo = logoImage
                    self.logoImageView.image = logoImage
                } else {
                    self.logoImageView.image = #imageLiteral(resourceName: "no_image")
                }
            }
            
            self.loadImageTask?.resume()
        } else {
            self.logoImageView.image = #imageLiteral(resourceName: "no_image")
        }
    }

}
