//
//  ChokeForumTests.swift
//  ChokeForumTests
//
//  Created by LiSim on 22/12/2016.
//  Copyright © 2016 AppLab Admin. All rights reserved.
//

import XCTest
@testable import ChokeForum

class ChokeForumTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWritingFullToServer() {
        let expectation = self.expectation(description: "Write Full Server")
        
        //let myTopic: Topic = Topic(title: "Choke is very leng zai", logo: UIImage(named: "ico")!)
        let myTopic: Topic = Topic(title: "Fuck everyone life")
        
        ForumLoader.sharedLoader.saveToServer(topic: myTopic) {
            (succeeded: Bool, error: Error?) -> Void in
            if succeeded {
                expectation.fulfill()
            } else {
                NSLog("Failed")
                if let error = error {
                    NSLog(error.localizedDescription)
                }
            }
        }
        
        self.waitForExpectations(timeout: 50, handler: nil)
    }
    
    func testReadingFromServer() {
        let expectation = self.expectation(description: "Read Server")
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let readURL: URL = URL(string: "data/Topic", relativeTo: baseURL)!
        
        let readTask: URLSessionDataTask = session.dataTask(with: readURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            var json: [String: Any] = try! JSONSerialization.jsonObject(with: data!) as! [String: Any]
            if let topicsJson: [[String: Any]] = json["data"] as? [[String: Any]] {
                for topicJson in topicsJson {
                    let topic: Topic = Topic(json: topicJson)!
                    NSLog("Topic \(topic.title)")
                    for comment in topic.comments {
                        NSLog("Comments\n\(comment.text)")
                    }
                }
            }
            expectation.fulfill()
        }
        
        readTask.resume()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testReadWrongImage() {
        let expectation = self.expectation(description: "Read Server")
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let readURL: URL = URL(string: "files/logo/abc.jpg", relativeTo: baseURL)!
        
        var urlRequest: URLRequest = URLRequest(url: readURL)
        urlRequest.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
        
        var a: String?
        
        let loadTask: URLSessionDataTask = session.dataTask(with: urlRequest) {
            (imageData: Data?, urlResponse: URLResponse?, error: Error?) -> Void in

            if let jsonDict = try? JSONSerialization.jsonObject(with: imageData!) as! [String: Any] {
                if let msg = jsonDict["message"] as? String {
                    print(msg)
                    a = msg
                }
            }
            
            expectation.fulfill()
        }
        
        loadTask.resume()
        
        if let _ = a {
            print("a is defined")
        } else {
            print("a not defined")
        }
        
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testWritingToServer() {
        let expectation = self.expectation(description: "Write Server")
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let url: URL = URL(string: "data/Topic", relativeTo: baseURL)!
        
        let topic: Topic = Topic(title: "Fuck my life")
        let jsonDict: [String : Any] = ["title" : topic.title]
        let jsonData: Data = try! JSONSerialization.data(withJSONObject: jsonDict)
        
        var urlRequest: URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let writeTask: URLSessionDataTask = session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            expectation.fulfill()
        }
        
        writeTask.resume()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testSavePicture() {
        let expectation = self.expectation(description: "Save Picture")
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let url: URL = URL(string: "files/logo/myPic2.jpg", relativeTo: baseURL)!
        
        let imageData: Data = UIImageJPEGRepresentation(UIImage(named: "test")!, 0.9)!
        
        //let myURL: URL = URL(string: "/Users/Choke/Desktop/ali.txt")!
        
        var urlRequest: URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        //urlRequest.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        //urlRequest.addValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        //urlRequest.httpBody = imageData
        //urlRequest.httpBody = FileManager.default.contents(atPath: "/Users/Choke/Desktop/ali.txt")
        
        let (boundary, formData) = HTTPContentTypeUtil.convertToFormData(fileName: "lala.jpg", mimeType: "image/jpeg", fileData: imageData)
        urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        //let uploadTask: URLSessionDataTask = session.dataTask(with: urlRequest) {
        //let uploadTask: URLSessionDataTask = session.uploadTask(with: urlRequest, fromFile: myURL) {
        let uploadTask: URLSessionDataTask = session.uploadTask(with: urlRequest, from: formData) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            var json: [String: Any] = try! JSONSerialization.jsonObject(with: data!) as! [String: Any]
            let httpCode: Int = (urlResponse as! HTTPURLResponse).statusCode
            NSLog("\(httpCode)")
            
            if let fileURL = json["fileURL"] {
                NSLog("File url is \(fileURL)")
            }
            if let errorCode = json["code"] {
                NSLog("Error code is \(errorCode)")
            }
            if let errorMsg = json["message"] {
                NSLog("Error message is \(errorMsg)")
            }
            
            expectation.fulfill()
        }
        
        uploadTask.resume()
        self.waitForExpectations(timeout: 50, handler: nil)
    }
    
    func testReadCommentFromServer() {
        let expectation = self.expectation(description: "Read Server")
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let readURL: URL = URL(string: "data/Comment", relativeTo: baseURL)!
        
        let readTask: URLSessionDataTask = session.dataTask(with: readURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            var json: [String: Any] = try! JSONSerialization.jsonObject(with: data!) as! [String: Any]
            if let commentsJson: [[String: Any]] = json["data"] as? [[String: Any]] {
                for commentJson in commentsJson {
                    let myComment: Comment = Comment(json: commentJson)!
                    NSLog("Test: \(myComment.text) Date: \(myComment.date)")
                }
            }
            expectation.fulfill()
        }
        
        readTask.resume()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testAddingCommentToTopic() {
        let expectation = self.expectation(description: "Add Comment")
        
        var topic: Topic!
        ForumLoader.sharedLoader.readAllTopicsFromServer {
            (topics: [Topic], error: Error?) -> Void in
            topic = topics[0]
            NSLog(topic.title)
            
        let comment: Comment = Comment(text: "Fuck you all")
        topic.comments.append(comment)
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let url: URL = URL(string: "data/Topic/\(topic.objectId!)", relativeTo: baseURL)!
        
        let jsonDict: [String : Any] = ["comments" : comment.jsonDict()]
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonDict)
        
        var urlRequest: URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "PUT"
        urlRequest.httpBody = jsonData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let writeTask: URLSessionDataTask = session.dataTask(with: urlRequest) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            //get object id and update the comment
            let json : [String : Any] = try! JSONSerialization.jsonObject(with: data!) as! [String : Any]
            comment.objectId = json["objectId"] as? String
            
            if let errorCode = json["code"] {
                NSLog("Error code is \(errorCode)")
            }
            if let errorMsg = json["message"] {
                NSLog("Error message is \(errorMsg)")
            }
            
            expectation.fulfill()
        }
        
        writeTask.resume()
        }
        self.waitForExpectations(timeout: 50, handler: nil)
    }
    
    func testObtainBooleanFromServer() {
        let expectation = self.expectation(description: "Read Server")
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "604A1C47-7D99-9B21-FF3A-5E7813858A00",
                                        "secret-key": "CEAA2581-5C30-9B0A-FF5B-0ECF5FE7A900",
                                        "application-type": "REST"]
        let session: URLSession = URLSession(configuration: config)
        let baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
        let readURL: URL = URL(string: "users/isvalidusertoken/825A61CE-C662-798F-FFBD-5575F0A70900", relativeTo: baseURL)!
        //let readURL: URL = URL(string: "users/isvalidusertoken/abc", relativeTo: baseURL)!
        
        let readTask: URLSessionDataTask = session.dataTask(with: readURL) {
            (data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            if let error = error {
                NSLog("The error is \(error)")
            } else {
                let success: String = String(data: data!, encoding: String.Encoding.utf8)!
                NSLog("The boolean is \(success)")
            }
            expectation.fulfill()
        }
        
        readTask.resume()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testUserDefaults() {
        User.instance.initialize(username: "sohai", objectId: "676767", userToken: "abc-efg")
        ForumLoader.sharedLoader.saveUserToDefaults()
        
        //ForumLoader.sharedLoader.deleteUserFromDefaults()
        
        XCTAssert(ForumLoader.sharedLoader.initializeUserFromDefaults())
        
        NSLog(User.instance.userToken)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
